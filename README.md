# Traefik playground

A simple docker-compose to play with Traefik (2.x)

[![Try in PWD](https://raw.githubusercontent.com/play-with-docker/stacks/master/assets/images/button.png)](https://labs.play-with-docker.com/?stack=https://gitlab.com/opencontent/traefik-playground/raw/master/docker-compose.yml)

## Requirements

 * [docker](https://docs.docker.com/install/)
 * [docker-compose](https://docs.docker.com/compose/install/)
 * [httpie](https://httpie.org/)

## How to start

    docker-compose up -d

then

    docker-compose ps

and finally

    docker-compose logs --tail 100 -f

Open the [traefik dashboard](http://localtest.me) and check the providers page,
check the association between frontend rules and backend services


## Play

### Automatic https redirect

An example service api.localtest.me is configured to automatically redirect to https protocol:

~~~shell
$ http get api.localtest.me

HTTP/1.1 302 Found
Content-Length: 5
Content-Type: text/plain; charset=utf-8
Date: Mon, 23 Dec 2019 15:04:32 GMT
Location: https://api.localtest.me/

Found

~~~

### Rate limiting

The same service is configured to limit the rate of the requests to 5 per second.

In a shell you can open a command that perform a lot of requests to the API:

    siege api.localtest.me

and then try:

~~~shell
$ http --verify=no get https://api.localtest.me

HTTP/1.1 429 Too Many Requests
Content-Length: 17
Content-Type: text/plain; charset=utf-8
Date: Mon, 23 Dec 2019 15:03:54 GMT
Retry-After: 10222
X-Retry-In: 2h50m22.268622665s

Too Many Requests

~~~


